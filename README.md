# Backend For A Student System #
The objective of this project was to create a backend student system using the Fastify Web Framework integrated with Node JS.

### Replication & Usage

- This project can be cloned using :
``` git clone https://gitlab.com/xosteve26/exeter_fastify.git ```

- Dependencies can be installed through the `package.json` file by running the `npm install` command in the terminal.

### Functions
The application can be made use of by using any API Verification tool such as POSTMAN, INSOMNIA etc.

- The route `http://localhost:5000/report` gives a response of all the recorded students. `[Route-Method : 'GET'] `


- The route `http://localhost:5000/add` adds a particular student.
Student details must be passed as key-value pairs in the 'Request Body'. `[Route-Method : 'POST']`

        Sample Request Body:
            {
                "studentName":"Mark",
                "studentID":"1",
                "subject1":78,
                "subject2":86,
                "subject3":94,
                "subject4":85,
                "subject5":75
            }

- The route `http://localhost:5000/delete` deletes a particular student from the records by passing in the 'studentID' as a key-value pair in the request body. `[Route-Method : 'DELETE']`

        Sample Request Body:
        {
            "studentID":"1"
        }

- The route `http://localhost:5000/update` updates the details of an existing user byt passing in the 'studentID' along with the students subject details in a key-value pair. `[Route-Method: 'POST']`

        Sample Request Body:
        {
            "studentID":"1",
            "subject1":78,
            "subject2":86,
            "subject3":94,
            "subject4":85,
            "subject5":75
        }



