let students=[]

const Student={
    type:'object',
    properties:{
      studentName:{type: 'string'},
      studentID:{type: 'string'},
      subject1:{type: 'number'},
      subject2:{type: 'number'},
      subject3:{type: 'number'},
      subject4:{type: 'number'},
      subject5:{type: 'number'},
    },
}

const getItemsOpts = {
    schema: {
      response: {
        200: {
          type: 'array',
          students: Student,
        },
      },
    },
 
  }

  
  const postItemOpts={
      schema:{
          body:{
            type:'object',
            required:["studentName","studentID","subject1","subject2","subject3","subject4","subject5"],
            properties:{
                studentName:{type:'string'},
                studentID:{type:'string'},
                subject1:{type:'number'},
                subject2:{type:'number'},
                subject3:{type:'number'},
                subject4:{type:'number'},
                subject5:{type:'number'}
            },
          },
          response:{
              201:Student
          }
      }
  }
  const delItemOpts = {
    schema: {
      response: {
        200: {
          type: 'object',
          properties:{
              message:{type:'string'},
          }
        },
      },
    },
 
  }

  const updateItemOpts={
      schema:{
          response:{
              200:Student,
          },
      },
  }

function itemRoutes(fastify, options, done){
    //Get The Records Of All Students
    fastify.get('/report',getItemsOpts,(req,reply)=>{
        reply.send(students)
    
    })
    
    //ADD A New Student 
    fastify.post('/add', postItemOpts,(req,reply)=>{
        const { studentName } = req.body
        const {studentID} = req.body
        const {subject1} = req.body
        const {subject2} = req.body
        const {subject3} = req.body
        const {subject4} = req.body
        const {subject5} = req.body

        const item={
            studentName,studentID,subject1,subject2,subject3,subject4,subject5
        }
        students=[...students,item]
        reply.code(201).send(item)
    })

    //Delete a student record using the "ID"
    fastify.delete('/delete', delItemOpts,(req,reply)=>{
        const{studentID}=req.body
        find_student_data=students.find((item)=>item.studentID===studentID)
        if (find_student_data != undefined){
            students = students.filter(item=> item.studentID !==studentID)
            reply.send({message:`The Student With the ID ${studentID} has been removed successfully`})
        }
        else{
            reply.send({message:`The Student With the ID ${studentID} doesn't exist`})
        }
        
        
    })
    //Update a student record py providing the necessary key:value pairs in the body 
    fastify.post('/update', updateItemOpts,(req,reply)=>{
        const{studentID}=req.body
        const{subject1}=req.body
        const{subject2}=req.body
        const{subject3}=req.body
        const{subject4}=req.body
        const{subject5}=req.body

        existing_student_data=students.find((item)=>item.studentID===studentID)

        if (req.body.studentName){
      
            const {studentName}=req.body 
            students=students.map((item)=>item.studentID === studentID ? {studentName,studentID,subject1,subject2,subject3,subject4,subject5}: item)  
        }
        else{
     
            const studentName=existing_student_data['studentName']
            students=students.map((item)=>item.studentID === studentID ? {studentName,studentID,subject1,subject2,subject3,subject4,subject5}: item)
        }

        updated_student_data=students.find((item)=>item.studentID===studentID)
        reply.send(updated_student_data)
    })
    done()
}


module.exports=itemRoutes